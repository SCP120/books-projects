FROM node:16-buster

WORKDIR /opt/book-projects

COPY package.json .

RUN apt-get install libcurl4

RUN npm install

COPY . .

EXPOSE 3014

CMD [ "npm","start" ]