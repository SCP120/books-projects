Các vấn đề đã gặp phải

Lỗi test khi build docker
do mac m1 chạy nền tảng arm64 nên khi build db lúc test sẽ bị lỗi dẫn đến test trên docker không chạy được (local chạy bình thường)

- Cách dùng : docker-compose up -d là lên
- Cấu trúc :
Books
books/ lấy danh sách books (tùy vào limit - mặc định là 100)
books/add - nhập dữ liệu json vào db
books/create - tạo ra 1 book mới
books/:bookId 
GET - lấy 1 book
POST - update 1 book
DELETE - xóa 1 book

Users - người mượn
users/ lấy danh sách users (tùy vào limit - mặc định là 100)
users/add - thêm 1 user mới
users/:userId
GET - lấy 1 user
POST - update 1 user
DELETE - xóa 1 user

Borrow - mượn- trả sách
borrow/ lấy danh sách borrow (tùy vào limit - mặc định là 100)
borrow/create - thêm 1 lần mượn ( borrower: người mượn , books - các sách được mượn , time: thời gian trả sách)
borrow/:borrowId
GET - lấy 1 bản ghi mượn sách được chỉ định
borrow/:borrowId/returnAll - trả sách

các công nghệ sử dụng
nodeJs, mongodb, jest, bull, express, bluebird,mongo-memory-server, docker
