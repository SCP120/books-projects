const _ = require('lodash')
const ObjectId = require('mongoose').Types.ObjectId
const moment = require('moment')

const getObjectId = (exports.getObjectId = function (obj) {
    const id = _.get(obj, '_id') || obj
    if (_.isString(id) && ObjectId.isValid(id)) return new ObjectId(id)
    return id
  })

exports.isEqual = function (a, b) {
    return _.isEqual(getObjectId(a), getObjectId(b))
  }

exports.dateToCron = (date) => {
    const minutes = moment(date).minutes();
    const hours = moment(date).hours();
    const days = moment(date).date();
    const months = moment(date).month() + 1;
    const dayOfWeek = moment(date).day();

    return `${minutes} ${hours} ${days} ${months} ${dayOfWeek}`;
};



