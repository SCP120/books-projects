require('dotenv').config()

const express = require("express")
const Mongoose = require("mongoose")
const BodyParser = require("body-parser")
const db = require("./config/db.js")
const routes = require('./app/routers')

const app = express()

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
if (process.env.NODE_ENV !== "test") {
    db.connect()
        .then(() => {
            app.listen(process.env.PORT, () => {
                console.log(`Server is listening to port ${process.env.PORT}`)
            })
        })
        .catch((err) => {
            console.log(err)
        })
}

app.use("/", routes)
module.exports = app