
const axios = require('axios');
const moment = require('moment')


exports.noticeBorrowProccess = async (job) => {
    const data ={
        content: `Bạn sắp đến hạn trả sách rồi nhé ! (${moment(job.data.returnDate).format('L')})`,
    }
   
    await axios.post('https://webhook.site/d7538244-7152-423f-a020-eed265870e9c',data)
}