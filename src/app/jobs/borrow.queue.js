const Bull = require('bull')
const {noticeBorrowProccess} =require('./borrow.process')
const helper = require('../../../src/config/helper.js')
const moment = require('moment')

const borrowQueue = new Bull('borrow-notice')
borrowQueue.process(noticeBorrowProccess)
exports.sendBorrowNotice = async (data) =>{
    const notiTime = moment(data.returnDate).subtract(1, 'days').toDate()
    const cron = helper.dateToCron(notiTime)
    await borrowQueue.add(data, {
        repeat: { cron: cron },
    })
}