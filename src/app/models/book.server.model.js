const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const BookSchema = new Schema({
    Name: String,
    RatingDist1: String,
    pagesNumber: Number,
    RatingDist4: String,
    RatingDistTotal: String,
    PublishMonth: Number,
    PublishDay: Number,
    Publisher:String,
    CountsOfReview:Number,
    PublishYear: Number,
    Language: String,
    Authors: String,
    Rating: Number,
    RatingDist2: String,
    RatingDist5: String,
    RatingDist3: String,
    ISBN: String,
    IsBorrowed: {
        type: Boolean,
        default: false
    },

});
module.exports = mongoose.model('Book', BookSchema);