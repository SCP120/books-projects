const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const BorrowSchema = new Schema({
    borrower: {
        type: Schema.ObjectId,
        ref: 'User',
        required: [true, 'Chưa có người mượn']
    },
    books: [
        {
            type: Schema.ObjectId,
            ref: 'Book',
        }
    ],
    created: {
        type: Date,
        default: Date.now,
    },
    updated: {
        type: Date,
    },
    returnDate: {
        type: Date,
    },
    isReturn: {
        type: Boolean,
        default: false
    }

});
module.exports = mongoose.model('Borrow', BorrowSchema);