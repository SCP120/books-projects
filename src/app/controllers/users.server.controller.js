
const mongoose = require('mongoose')
const User = require('../models/user.server.model.js')

exports.create = async (req, res) => {
    const user = new User(req.body)
    try {
        await user.save()
        res.jsonp(user)
    } catch (e) {
        res.status(400).send(e)
    }
}

exports.list = async (req, res) => {
    const limit = req.query.limit || 100
    const users = await User.find({
    }).sort('-createdAt').limit(limit)
    res.jsonp(users)
}

exports.update = async (req, res) => {
    const user = req.user
    const body = req.body
    const updatedUser = Object.assign(user, body)
    await updatedUser.save()
    res.jsonp(updatedUser)
}

exports.read = function (req, res) {
    res.jsonp(req.user)
  }

exports.delete = async (req, res) => {
    const user = req.user
    user.remove(function (err) {
        if (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err),
          })
        } else {
            res.jsonp('Successfully deleted')
        }
      })
   
}


exports.userById = (req, res, next, id) => {
    User.findById(id)
        .exec(function (err, user) {
            if (err) {
                next(err)
                return
            }
            if (!user) {
                next(new Error(`Failed to load User ${id}`))
                return
            }
            req.user = user
            next()
        })
}