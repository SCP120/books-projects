const fs = require('fs');
const mongoose = require('mongoose')
const ObjectId = mongoose.Types.ObjectId
const Book = require('../models/book.server.model.js')

exports.addToTable = async (req, res) => {
    fs.readFile('data/books/book1-100k.json', 'utf8', function (err, data) {
        if (err) throw err;
        const json = JSON.parse(data);
        const chunkSize = 1000;
        for (let i = 0; i < json.length; i += chunkSize) {
            const chunk = json.slice(i, i + chunkSize)
            Book.insertMany(chunk, function(err, doc) {
                if(err) throw err;
            });
        }
        res.jsonp('Successfully added to table')
    });
}

exports.create = async (req, res) => {
    const book = new Book(req.body)
    try {
        await book.save()
        res.jsonp(book)
    } catch (e) {
        res.status(400).send(e)
    }
}

exports.list = async (req, res) => {
    const limit = req.query.limit || 100
    const books = await Book.find({
    }).sort('-createdAt').limit(limit)
    res.jsonp(books)
}

exports.update = async (req, res) => {
    const book = req.book
    const body = req.body
    const updatedBook = Object.assign(book, body)
    await updatedBook.save()
    res.jsonp(updatedBook)
}

exports.read = function (req, res) {
    res.jsonp(req.book)
  }

exports.delete = async (req, res) => {
    const book = req.book
    book.remove(function (err) {
        if (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err),
          })
        } else {
            res.jsonp('Successfully deleted')
        }
      })
   
}


exports.bookById = (req, res, next, id) => {
    Book.findById(id)
        .exec(function (err, book) {
            if (err) {
                next(err)
                return
            }
            if (!book) {
                next(new Error(`Failed to load Book ${id}`))
                return
            }
            req.book = book
            next()
        })
}