const fs = require('fs');
const mongoose = require('mongoose')
const ObjectId = mongoose.Types.ObjectId
const Borrow = require('../models/borrow.server.model.js')
const Book = require('../models/book.server.model.js')
const bluebird = require('bluebird')
const _ = require('lodash')
const moment = require('moment')
const helper = require('../../../src/config/helper.js')
const notice = require('../jobs/borrow.queue')

exports.create = async (req, res) => {
    const { borrower, books, time } = req.body
    const _books = []
    await bluebird.each(books, (book) => {
        if (!book.IsBorrowed) {
            Book.updateOne({ _id: helper.getObjectId(book) }, { $set: { IsBorrowed: true } })
            _books.push(book)
        }
    })
    const borrow = await Borrow({
        borrower: borrower,
        books: _books,
        returnDate: moment(time)
    })
    try {
        await borrow.save()
        if (process.env.NODE_ENV === "development") {
            notice.sendBorrowNotice(borrow)
        }
         res.jsonp(borrow)
    } catch (e) {
        res.status(400).send(e)
    }
}

exports.list = async (req, res) => {
    const limit = req.query.limit || 100
    const borrow = await Borrow.find({
        isReturn: false
    }).sort('-createdAt').limit(limit)
    res.jsonp(borrow)
}

exports.read = function (req, res) {
    res.jsonp(req.borrow)
}

exports.return = async (req, res) => {
    const borrow = req.borrow
    await bluebird.each(borrow.books, async (book) => {
        await Book.updateOne({ _id: book._id }, { $set: { IsBorrowed: false } })
    })
    borrow.updated = Date.now()
    borrow.isReturn = true

    borrow.save(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err),
            })
        } else {
            res.jsonp(borrow)
        }
    })

}


exports.borrowById = (req, res, next, id) => {
    Borrow.findById(id)
        .populate('books')
        .exec(function (err, borrow) {
            if (err) {
                next(err)
                return
            }
            if (!borrow) {
                next(new Error(`Failed to load ${id}`))
                return
            }
            req.borrow = borrow
            next()
        })
}