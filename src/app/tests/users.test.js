const request = require("supertest")
const app = require("../../../src/index")
const db = require('./config/database')

beforeAll(async () => await db.connect())
afterAll(async () => await db.clear())
afterAll((done => {
    db.close()
    done()
}))

describe("POST /users/create", () => {
    it("POST", async () => {
      const res = await request(app).post("/users/create").send({
        username: 'TestName',
      })
      expect(res.statusCode).toEqual(200)
      expect(res.body).toBeTruthy()
      expect(res.body.username).toEqual('TestName')
    })
})

describe("GET /users", () => {
  it("GET", async () => {
    const res = await request(app).get("/users")
    expect(res.statusCode).toEqual(200)
    expect(res.body).toBeTruthy()
  })
})



describe(" /:userId", () => {
  let data
  it("Create test user", async () => {
    data = await request(app).post("/users/create").send({
        username: 'test',
        password: '123456'
    })
    expect(data.statusCode).toEqual(200)
    expect(data.body).toBeTruthy()
    expect(data.body.username).toEqual('test')
  })
  it("GET", async () => {
    const res = await request(app).get("/users/" + data.body._id)
    expect(res.statusCode).toEqual(200)
    expect(res.body).toBeTruthy()
    expect(res.body.username).toEqual('test')
  })
  it("POST", async () => {
    const res = await request(app).post(`/users/${data.body._id}`).send({
      username: 'TestName1',
    })
    expect(res.statusCode).toEqual(200)
    expect(res.body).toBeTruthy()
    expect(res.body.username).toEqual('TestName1')
  })
  it("DELETE", async () => {
    const res = await request(app).delete(`/users/${data.body._id}`)
    expect(res.statusCode).toEqual(200)
    expect(res.body).toBeTruthy()
  })



})

