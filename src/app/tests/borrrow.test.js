const request = require("supertest")
const app = require("../../../src/index")
const db = require('./config/database')
const moment = require('moment')

beforeAll(async () =>  await db.connect())
afterAll(async () => await db.clear())
afterAll((async () => {
    await db.close()
}))

let data
describe("POST /borrow/create", () => {
    let user
    let book
    it("Create test book", async () => {
        book = await request(app).post("/books/create").send({
            Name: 'testBook',
        })
        expect(book.statusCode).toEqual(200)
        expect(book.body).toBeTruthy()
        expect(book.body.Name).toEqual('testBook')
    })
    it("Create test user", async () => {
        user = await request(app).post("/users/create").send({
            username: 'test',
            password: '123456'
        })
        expect(user.statusCode).toEqual(200)
        expect(user.body).toBeTruthy()
        expect(user.body.username).toEqual('test')
    })
    it("POST", async () => {
        data = await request(app).post("/borrow/create").send({
            borrower: user.body,
            books: [book.body],
            time: moment()
        })
        expect(data.statusCode).toEqual(200)
        expect(data.body).toBeTruthy()
        expect(data.body.borrower).toEqual(user.body._id)
    })
})

describe("GET /borrow", () => {
    it("GET", async () => {
        const res = await request(app).get("/borrow")
        expect(res.statusCode).toEqual(200)
        expect(res.body).toBeTruthy()
    })
})

describe(" GET /borrow/:borrowId", () => {
    it("GET", async () => {
        const res = await request(app).get("/borrow/" + data.body._id)
        expect(res.statusCode).toEqual(200)
        expect(res.body).toBeTruthy()
    })
})

describe(" POST /borrow/:borrowId/returnAll", () => {
    it("POST", async () => {
        const res = await request(app).post("/borrow/" + data.body._id + "/returnAll")
        expect(res.statusCode).toEqual(200)
        expect(res.body).toBeTruthy()
    })
})

