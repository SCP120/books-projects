const mongoose = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');



exports.connect = async () => {
    const mongod = await MongoMemoryServer.create({ binary: {  } });
    const uri = mongod.getUri()
    const mongooseOpts = {
        useNewUrlParser: true,
       
        useUnifiedTopology: true,
     
    }
    await mongoose.connect(uri, mongooseOpts)
}

exports.close = async () => {
    await mongoose.connection.dropDatabase()
    await mongoose.connection.close()
};

exports.clear = async () => {
    const collections = mongoose.connection.collections;
    for (const key in collections) {
        await collections[key].deleteMany({})
    }
}