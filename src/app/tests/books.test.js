const request = require("supertest")
const app = require("../../../src/index")
const db = require('./config/database')

beforeAll(async () => await db.connect())
afterAll(async () => await db.clear())
afterAll((done => {
  db.close()
  done()
}))

describe("GET /books/add", () => {
  jest.setTimeout(15000)
  it("successful - add", async () => {
    const res = await request(app).get("/books/add")
    expect(res.statusCode).toEqual(200)
    expect(res.body).toBeTruthy()
  })
})


describe("GET /books", () => {
  it("GET", async () => {
    const res = await request(app).get("/books")
    expect(res.statusCode).toEqual(200)
    expect(res.body).toBeTruthy()
  })
})

describe("POST /books/create", () => {
  it("POST", async () => {
    const res = await request(app).post("/books/create").send({
      Name: 'TestName',
    })
    expect(res.statusCode).toEqual(200)
    expect(res.body).toBeTruthy()
    expect(res.body.Name).toEqual('TestName')
  })
})

describe(" /:bookId", () => {
  let data
  it("Create test book", async () => {
    data = await request(app).post("/books/create").send({
      Name: 'test',
    })
    expect(data.statusCode).toEqual(200)
    expect(data.body).toBeTruthy()
    expect(data.body.Name).toEqual('test')
  })
  it("GET", async () => {
    const res = await request(app).get("/books/" + data.body._id)
    expect(res.statusCode).toEqual(200)
    expect(res.body).toBeTruthy()
    expect(res.body.Name).toEqual('test')
  })
  it("POST", async () => {
    const res = await request(app).post(`/books/${data.body._id}`).send({
      Name: 'TestName1',
    })
    expect(res.statusCode).toEqual(200)
    expect(res.body).toBeTruthy()
    expect(res.body.Name).toEqual('TestName1')
  })
  it("DELETE", async () => {
    const res = await request(app).delete(`/books/${data.body._id}`)
    expect(res.statusCode).toEqual(200)
    expect(res.body).toBeTruthy()
  })



})

