const express = require('express')

const router = express.Router()

module.exports = router

router.use('/books', require('../app/routers/books.server.routes'))
router.use('/users', require('../app/routers/users.server.routes'))
router.use('/borrow', require('../app/routers/borrow.server.routes'))
