

const express = require('express')
const users = require('../controllers/users.server.controller.js')
const router = express.Router()

module.exports = router

router.get('', users.list)

router.post('/create', users.create)

router.route('/:userId')
    .get(users.read)
    .post(users.update)
    .delete(users.delete)

router.param('userId', users.userById)
