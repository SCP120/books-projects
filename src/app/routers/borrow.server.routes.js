

const express = require('express')
const borrow = require('../controllers/borrow.server.controller.js')
const router = express.Router()

module.exports = router

router.get('', borrow.list)


router.post('/create', borrow.create)

router.get('/:borrowId', borrow.read)
router.post('/:borrowId/returnAll', borrow.return)
router.param('borrowId', borrow.borrowById)
