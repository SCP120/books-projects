

const express = require('express')
const books = require('../controllers/books.server.controller.js')
const router = express.Router()

module.exports = router

router.get('', books.list)

router.get('/add', books.addToTable)

router.post('/create', books.create)

router.route('/:bookId')
    .get(books.read)
    .post(books.update)
    .delete(books.delete)

router.param('bookId', books.bookById)
